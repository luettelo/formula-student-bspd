# Formula Student BSPD

A FSG and FSAE compliant non-programmable Break System Plausability Device for Formula Student usage
___

## Features

- Analog comparator based input stage with user settable voltage tresholds for break and gas pedal signals
- Rest of the system is based on digital logigs eliminating nasty RC glitches
- Solid state power relay to disconnect series safety signal
- Two designs:
  - Normal small form factor design without debug state LEDs
  - Debug version with buffered LEDs to indicate different states of the BSPD signaling
- Wide supply voltage range 16V to ~4V
- Easy to assemble by hand with soldering iron without special SMD assembly tools.

___

## Hot to use

- Reguires a KiCad 6 (or 5.99)
- All the nessesary symbols, footprints and 3D models are part of the project (library folder) and therefore the project is self sufficient
  - Symbol, footprint and standard 3D models are copied from standard KiCad libraries
  - Also custom symbols, footprints and 3D-models are used (bspd_custom) under library folder
  - Depending on users KiCad and system configurations it may be needed to alter paths of the project libraries

___

## How to operate

- Build the BSPD
- Test the device on lab conditions in case of problems during assembling
- Using multimeter (or oscilloscope or similiar) measure what are the voltages from the brake and throttle signal lines to meet rule requirements
  - Supply voltage of the internal logics and op-amps of 3.3V can be limiting factor on some systems. Therefore input scaler can be used to decrese voltage levels coming into the BSPD. Both input signals feature input scalers. Bypass by using 0R resistors. Depending on design a small pull down resitor may be needed and should therefore be populated.
- Set these values as the threshold for the comparator stages using resistor division calculation Vout = 3.3V * (R2 / (R1 + R2))

___

## Normal version

The normal version is small two sided PCB with both sides populated with components. Fron side of the PCB includes all the digital logics whereas the back side contains all the user configurable components. The design incorporates M3 mounting holes and strain relief for the cables.
  
![Schematics](designs/normal_version/docs/normal_version_schema.png)

Front side | Back side
:---: | :---:
![Schematics](designs/normal_version/docs/front_raytrace.png) | ![Schematics](designs/normal_version/docs/back_raytrace.png)

___

## Debug version

The debug version is a larger single sided version of the normal version with seperatly buffered indication LEDs to signal which part of the circuit is triggered. Proven to be very useful outside competition setting while testing.

DISCLAIMER: It can be arguet that since LEDs are seperately buffered this design is compatible with FSG/FSAE rules. There is a lot of room for the judges to interpret the rules and therefore there is no clear answer.
  
![Schematics](designs/debug_version/docs/debug_version_schema.png)

Front side | Back side
:---: | :---:
![Schematics](designs/debug_version/docs/front_raytrace.png) | ![Schematics](designs/debug_version/docs/back_raytrace.png)
___

## Licensing

MIT License
